﻿namespace WWS
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            treeView1 = new TreeView();
            textBox1 = new TextBox();
            textBox2 = new TextBox();
            textBox3 = new TextBox();
            button1 = new Button();
            progressBar1 = new ProgressBar();
            cbEditFileName = new CheckBox();
            tbNewFileName = new TextBox();
            SuspendLayout();
            // 
            // treeView1
            // 
            treeView1.CheckBoxes = true;
            treeView1.Location = new Point(11, 53);
            treeView1.Name = "treeView1";
            treeView1.Size = new Size(346, 384);
            treeView1.TabIndex = 0;
            treeView1.AfterCheck += node_AfterCheck;
            // 
            // textBox1
            // 
            textBox1.Location = new Point(11, 21);
            textBox1.Name = "textBox1";
            textBox1.ReadOnly = true;
            textBox1.Size = new Size(346, 27);
            textBox1.TabIndex = 1;
            textBox1.Click += textBox1_Click;
            // 
            // textBox2
            // 
            textBox2.Location = new Point(363, 21);
            textBox2.Name = "textBox2";
            textBox2.ReadOnly = true;
            textBox2.Size = new Size(316, 27);
            textBox2.TabIndex = 2;
            textBox2.Click += textBox2_Click;
            // 
            // textBox3
            // 
            textBox3.Location = new Point(363, 86);
            textBox3.Multiline = true;
            textBox3.Name = "textBox3";
            textBox3.ScrollBars = ScrollBars.Both;
            textBox3.Size = new Size(425, 281);
            textBox3.TabIndex = 3;
            // 
            // button1
            // 
            button1.Location = new Point(363, 408);
            button1.Name = "button1";
            button1.Size = new Size(425, 29);
            button1.TabIndex = 4;
            button1.Text = "Save";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // progressBar1
            // 
            progressBar1.Location = new Point(363, 373);
            progressBar1.Name = "progressBar1";
            progressBar1.Size = new Size(425, 29);
            progressBar1.TabIndex = 5;
            // 
            // cbEditFileName
            // 
            cbEditFileName.AutoSize = true;
            cbEditFileName.Location = new Point(685, 24);
            cbEditFileName.Name = "cbEditFileName";
            cbEditFileName.Size = new Size(101, 24);
            cbEditFileName.TabIndex = 6;
            cbEditFileName.Text = "Edit Name";
            cbEditFileName.UseVisualStyleBackColor = true;
            cbEditFileName.CheckedChanged += cbEditFileName_CheckedChanged;
            // 
            // tbNewFileName
            // 
            tbNewFileName.Location = new Point(363, 53);
            tbNewFileName.Name = "tbNewFileName";
            tbNewFileName.Size = new Size(316, 27);
            tbNewFileName.TabIndex = 7;
            tbNewFileName.Visible = false;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 451);
            Controls.Add(tbNewFileName);
            Controls.Add(cbEditFileName);
            Controls.Add(progressBar1);
            Controls.Add(button1);
            Controls.Add(textBox3);
            Controls.Add(textBox2);
            Controls.Add(textBox1);
            Controls.Add(treeView1);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TreeView treeView1;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private Button button1;
        private ProgressBar progressBar1;
        private CheckBox cbEditFileName;
        private TextBox tbNewFileName;
    }
}