﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WWS.Helpers
{
    public static class ScriptsSaver
    {
        public static List<string> GetAllPaths(TreeNodeCollection nodes, string defaultPath, string fileName)
        {
            List<string> paths = new List<string>();

            foreach (TreeNode node in nodes)
            {
                GetPaths(node, defaultPath, paths, fileName);
            }

            return paths;
        }

        private static void GetPaths(TreeNode node, string defaultPath, List<string> paths, string fileName)
        {
            foreach (TreeNode treeNode in node.Nodes)
            {
                if (node.Checked)
                {
                    GetPaths(treeNode, defaultPath, paths, fileName);
                }
            }

            if (node.Checked && node.Nodes.Count == 0)
            {
                paths.Add(defaultPath + "\\" + node.FullPath + "\\" + fileName);
            }
        }
    }
}
