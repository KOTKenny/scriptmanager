﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WWS.SpecialSettings
{
    public static class ExcludedDirectories
    {
        public static ICollection<string> Directories = new List<string>()
        {
            {"Released"},
            {"Archive"},
            {"Splitted"},
            {"Initial DB backup"},
            {"Initial BD backup"},
            {"MigrationScripts"},
            {"CommonTools"},
            {"MigrationTool"}
        };
    }
}
