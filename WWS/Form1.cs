using WWS.Helpers;
using WWS.SpecialSettings;

namespace WWS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog openFileDlg = new System.Windows.Forms.FolderBrowserDialog();
            var result = openFileDlg.ShowDialog();
            if (result.ToString() != string.Empty)
            {
                textBox1.Text = openFileDlg.SelectedPath;

                if (!String.IsNullOrEmpty(textBox1.Text))
                    LoadTreeView(textBox1.Text);
            }
        }

        private void LoadTreeView(string path)
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
            treeView1.Nodes.Clear();

            LoadSubFolders(di, treeView1.Nodes);
        }

        private void LoadSubFolders(DirectoryInfo di, TreeNodeCollection node)
        {
            System.IO.DirectoryInfo[] ds = di.GetDirectories("*.*", System.IO.SearchOption.TopDirectoryOnly);

            if (ds.Length > 0)
            {
                foreach (DirectoryInfo d in ds)
                {
                    if (ExcludedDirectories.Directories.Contains(d.Name))
                    {
                        continue;
                    }

                    node.Add(d.Name);

                    LoadSubFolders(d, node[node.Count - 1].Nodes);
                }
            }
        }

        // Updates all child tree nodes recursively.
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        // NOTE   This code can be added to the BeforeCheck event handler instead of the AfterCheck event.
        // After a tree node's Checked property is changed, all its child nodes are updated to the same value.
        private void node_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // The code only executes if the user caused the checked state to change.
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    /* Calls the CheckAllChildNodes method, passing in the current 
                    Checked value of the TreeNode whose checked state changed. */
                    e.Node.ExpandAll();
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDlg = new System.Windows.Forms.OpenFileDialog();
            var result = openFileDlg.ShowDialog();
            if (result.ToString() != string.Empty)
            {
                textBox2.Text = openFileDlg.SafeFileName;
                if (String.IsNullOrEmpty(textBox2.Text))
                    return;

                using (StreamReader file = new StreamReader(openFileDlg.FileName))
                {
                    int counter = 0;
                    string ln;
                    List<string> fileTxt = new List<string>();

                    while ((ln = file.ReadLine()) != null)
                    {
                        fileTxt.Add(ln);
                        counter++;
                    }
                    file.Close();

                    textBox3.Lines = fileTxt.ToArray();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var paths = ScriptsSaver.GetAllPaths(treeView1.Nodes, textBox1.Text, textBox2.Text);

            foreach (var path in paths)
            {
                if (File.Exists(path))
                {
                    File.WriteAllText(path, textBox3.Text);

                    if (cbEditFileName.Checked && !String.IsNullOrEmpty(tbNewFileName.Text))
                    {
                        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
                        File.Move(path, di.Parent.FullName + "\\" + tbNewFileName.Text, true);
                    }
                }
            }
        }

        private void cbEditFileName_CheckedChanged(object sender, EventArgs e)
        {
            if(cbEditFileName.Checked)
            {
                tbNewFileName.Text = textBox2.Text;
                tbNewFileName.Visible = true;
            }
            else
            {
                tbNewFileName.Text = String.Empty;
                tbNewFileName.Visible = false;
            }
        }
    }
}